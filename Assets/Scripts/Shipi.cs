using UnityEngine;
using System.Collections;

public class CycleMove : MonoBehaviour
{

    public float speed = 1; //скорость
    public float dist = 1; // расстояние
    private Transform shipiGroup;
    // Use this for initialization
    void Start()
    {
        shipiGroup = transform.GetChild(0);
    }

    // Update is called once per frame
    void Update()
    {
        float posY = Mathf.Sin(Time.time * speed) * dist;
        Vector3 pos = shipiGroup.transform.localPosition;
        shipiGroup.transform.localPosition = new Vector3(pos.x, posY, pos.z);
    }
}

