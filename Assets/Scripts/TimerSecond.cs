using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerSecond : MonoBehaviour
{

    public float timeStart;
    public Text textTimer;


    void Start()
    {
        textTimer.text = timeStart.ToString("F2");
    }

    // Update is called once per frame
    void Update()
    {
        timeStart += Time.deltaTime;
        textTimer.text = timeStart.ToString("F2");
    }
}
