using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RocketSecond : MonoBehaviour
{

    Rigidbody rigidBody;
    enum State  {Playing,Dead,NextLevel};
    State state = State.Playing;
    private static int currentLevelIndex = 1;
    private static int finishLevelIndex = 3;
    public int coins;


    // Start is called before the first frame update
    void Start()

    {
        print("Hello world");
        Debug.Log("Start");

        state = State.Playing;
        rigidBody = GetComponent<Rigidbody>();



    }

    // Update is called once per frame
    void Update()
    {
        if (state == State.Playing)
        {

            Launch();

            Rotation();
        }

    }

    private void OnCollisionEnter(Collision collision)

    {
        Debug.Log("OnCollisionEnter:"+collision.gameObject.tag);

        if (state != State.Playing )
        {
            return;
        }
        switch(collision.gameObject.tag)
        { 
            case "Friendly":
                print("OK");
                break;
            case "Finish":
                state = State.NextLevel;
                LoadNextLevel();
                break;
            case "Battery":
                print("PlusEnergy");
                break;
            default:
                state = State.Dead;
                print("RocketBoom!");
                LoadFirstLevel();
                break;
        }
    }

   



void LoadNextLevel() // Finish
    {
        
        currentLevelIndex = currentLevelIndex + 1;

        
        Debug.Log("OnCollisionEnter:" + currentLevelIndex);

        if (currentLevelIndex > finishLevelIndex )
        {
            currentLevelIndex = 0; 
        }
        SceneManager.LoadScene(currentLevelIndex);
        
    }

    void LoadFirstLevel()  // Lose
    {
        Debug.Log("LoadFirstLevel");
        SceneManager.LoadScene(1);
        currentLevelIndex = 1;
    }
    void Launch()


    {
        if (Input.GetKey(KeyCode.Space))
        {

            rigidBody.AddRelativeForce(Vector3.up);

        }




    }
    void Rotation()

    {
        rigidBody.freezeRotation = true;

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward);
        }

        else if (Input.GetKey(KeyCode.D))

        {
            transform.Rotate(-Vector3.forward);
        }
        rigidBody.freezeRotation = false;


    }
}





  

